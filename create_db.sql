-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema shift_planner
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema shift_planner
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `shift_planner` DEFAULT CHARACTER SET utf8 COLLATE utf8_czech_ci ;
USE `shift_planner` ;

-- -----------------------------------------------------
-- Table `shift_planner`.`user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shift_planner`.`user_role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `display_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shift_planner`.`employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shift_planner`.`employee` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(150) NOT NULL,
  `password` VARCHAR(64) NOT NULL,
  `firstname` VARCHAR(35) NOT NULL,
  `lastname` VARCHAR(35) NOT NULL,
  `active` TINYINT(1) NOT NULL DEFAULT 1,
  `titles_before` VARCHAR(20) NULL,
  `titles_after` VARCHAR(20) NULL,
  `user_role_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_user_user_role_idx` (`user_role_id` ASC),
  CONSTRAINT `fk_user_user_role`
    FOREIGN KEY (`user_role_id`)
    REFERENCES `shift_planner`.`user_role` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shift_planner`.`office`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shift_planner`.`office` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `active` TINYINT(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shift_planner`.`shift_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shift_planner`.`shift_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(40) NOT NULL,
  `start` DATETIME NOT NULL,
  `end` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shift_planner`.`shift`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shift_planner`.`shift` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `working_hours_from` DATETIME NOT NULL,
  `working_hours_to` DATETIME NOT NULL,
  `office_id` INT NOT NULL,
  `nurse` INT NULL,
  `doctor` INT NULL,
  `shift_type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_shift_office1_idx` (`office_id` ASC),
  INDEX `fk_shift_user1_idx` (`nurse` ASC),
  INDEX `fk_shift_user2_idx` (`doctor` ASC),
  INDEX `fk_shift_shift_type1_idx` (`shift_type_id` ASC),
  CONSTRAINT `fk_shift_office1`
    FOREIGN KEY (`office_id`)
    REFERENCES `shift_planner`.`office` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shift_user1`
    FOREIGN KEY (`nurse`)
    REFERENCES `shift_planner`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shift_user2`
    FOREIGN KEY (`doctor`)
    REFERENCES `shift_planner`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shift_shift_type1`
    FOREIGN KEY (`shift_type_id`)
    REFERENCES `shift_planner`.`shift_type` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shift_planner`.`shift_month_limit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shift_planner`.`shift_month_limit` (
  `employee_id` INT NOT NULL,
  `shift_type_id` INT NOT NULL,
  `quantity` INT NOT NULL DEFAULT '-1',
  INDEX `fk_shift_limit_employee1_idx` (`employee_id` ASC),
  INDEX `fk_shift_limit_shift_type1_idx` (`shift_type_id` ASC),
  PRIMARY KEY (`employee_id`, `shift_type_id`),
  CONSTRAINT `fk_shift_limit_employee1`
    FOREIGN KEY (`employee_id`)
    REFERENCES `shift_planner`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shift_limit_shift_type1`
    FOREIGN KEY (`shift_type_id`)
    REFERENCES `shift_planner`.`shift_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `shift_planner`.`deadline`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `shift_planner`.`deadline` (
  `id` DATE NOT NULL,
  `finished_until` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `shift_planner`.`user_role`
-- -----------------------------------------------------
START TRANSACTION;
USE `shift_planner`;
INSERT INTO `shift_planner`.`user_role` (`id`, `name`, `display_name`) VALUES (1, 'admin', 'Administátor');
INSERT INTO `shift_planner`.`user_role` (`id`, `name`, `display_name`) VALUES (2, 'doctor', 'Doktor');
INSERT INTO `shift_planner`.`user_role` (`id`, `name`, `display_name`) VALUES (3, 'nurse', 'Zdravotní sestra');

COMMIT;


-- -----------------------------------------------------
-- Data for table `shift_planner`.`employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `shift_planner`;
INSERT INTO `shift_planner`.`employee` (`id`, `email`, `password`, `firstname`, `lastname`, `active`, `titles_before`, `titles_after`, `user_role_id`) VALUES (1, 'admin@admin.com', '$2y$10$EvkQvT4JN9N8tUw9inbr8u6dI0128GfpZVTVl./wCb73V0.oBpmvi', 'Admin', 'Administrátor', 1, NULL, NULL, 1);

COMMIT;

