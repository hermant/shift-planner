# Plánovač směn
## Instalace
1. Vytvořit databázi pomocí skriptu `create_db.sql` na MariaDB databázi
2. Zkopírovat vše ze složky `src/` na PHP webserver ve verzi 7.1 a výš
3. Nastavit složku `www/` jako root directory, pokud to není možné z důvodu, že server běží na sdíleném hostingu(wedos, forpsi), tak nahradit `.htaccess` souborem `.htaccess-shared`
4. Nainstalovat všechny potřebné závilosti pomocí utility `composer` příkazem `composer update`
5. V souboru `app/config/common.neon` změnit údaje k databázi
6. Vymazat ve složce `temp/` vše kromě `.gitignore`
## Příhlášení do systému jako administrátor
Email `admin@admin.com`, heslo `Admin12345`

Silně doporučuji si ihned po nainstalování změnit údaje k administrátorskému účtu nebo daný účet deaktivovat!

## Update
1. Aktualizovat databázi pomocí skriptu `update_db_v2.sql` na MariaDB databázi
2. Synchronizovat soubory ve složce `src/`
3. V souboru `app/config/common.neon` změnit údaje k databázi
4. Aktualizovat všechny potřebné závilosti pomocí utility `composer` příkazem `composer update`
5. Vymazat ve složce `temp/` vše kromě `.gitignore`

