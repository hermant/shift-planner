<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\DuplicateNameException;
use App\Model\ShiftMonthLimitManager;
use App\Model\ShiftTypeManager;
use App\Model\UserRoleManager;
use Nette;
use App\Model\UserManager;
use Nette\Application\UI\Form;

class UserCreateFactory
{
    /** @var FormFactory */
    private $factory;

    /** @var UserManager */
    private $userManager;

    /** @var UserRoleManager */
    private $userRoleManager;

    /** @var ShiftTypeManager */
    private $shiftTypeManager;

    /** @var ShiftMonthLimitManager */
    private $shiftMonthLimitManager;


    public function __construct(FormFactory $factory, UserManager $userManager, ShiftTypeManager $shiftTypeManager, UserRoleManager $userRoleManager, ShiftMonthLimitManager $shiftMonthLimitManager)
    {
        $this->factory = $factory;
        $this->userManager = $userManager;
        $this->userRoleManager = $userRoleManager;
        $this->shiftTypeManager = $shiftTypeManager;
        $this->shiftMonthLimitManager = $shiftMonthLimitManager;
    }

    public function create(callable $onSuccess): Form
    {
        $form = $this->factory->create();
        $form->addEmail('email')
            ->setRequired();

        $form->addPassword('password')
            ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 8)
            ->addRule(Form::PATTERN, 'Musí obsahovat alespoň 1 číslici', '.*[0-9].*')
            ->addRule(Form::PATTERN, 'Musí obsahovat alespoň 1 velké písmeno', '.*[A-Z].*')
            ->addRule(Form::PATTERN, 'Musí obsahovat alespoň 1 malé písmeno', '.*[a-z].*')
            ->setRequired();

        $form->addSelect('role', null, $this->userRoleManager->getTable()->fetchPairs('id', 'display_name'))
            ->setRequired();

        $form->addText('firstname')
            ->setRequired();

        $form->addText('lastname')
            ->setRequired();

        $form->addText('titles_before');

        $form->addText('titles_after');

        foreach($this->shiftTypeManager->getActive() as $type) {
            $form->addInteger('limit_' . (string)$type->id)->setRequired()->setDefaultValue(-1);
            //$form->addCheckbox('check' . (string)$type->id);
        }

        $form->addSubmit('submit');

        $form->onSuccess[] = function (Form $form, \stdClass $values) use ($onSuccess): void {
            try {
                $user = $this->userManager->add($values->email, $values->password, $values->firstname, $values->lastname, $values->titles_before, $values->titles_after, $values->role);
                foreach($this->shiftTypeManager->getActive() as $type) {
                    $this->shiftMonthLimitManager->update((int)$user->id, (int)$type->id, ((array)$values)[(string)'limit_' . $type->id]);
                }
            } catch (DuplicateNameException $e) {
                $form['email']->addError('Tento email již někdo používá.');
                return;
            }
            $onSuccess();
        };

        return $form;
    }
}