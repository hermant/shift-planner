<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\OfficeManager;
use App\Model\ShiftManager;
use App\Model\ShiftOverlappingException;
use App\Model\ShiftTypeManager;
use App\Model\ShiftVersionManager;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;

class ShiftCreateFormFactory
{

    use Nette\SmartObject;

    /** @var FormFactory */
    private $factory;

    /** @var ShiftManager */
    private $shiftManager;

    /** @var OfficeManager */
    private $officeManager;

    /** @var ShiftTypeManager */
    private $shiftTypeManager;

    /** @var ShiftVersionManager */
    private $shiftVersionManager;

    public function __construct(FormFactory $factory, ShiftManager $shiftManager, OfficeManager $officeManager, ShiftTypeManager $shiftTypeManager, ShiftVersionManager  $shiftVersionManager)
    {
        $this->factory = $factory;
        $this->shiftManager = $shiftManager;
        $this->officeManager = $officeManager;
        $this->shiftTypeManager = $shiftTypeManager;
        $this->shiftVersionManager = $shiftVersionManager;
    }

    public function create(callable $onSuccess): Form
    {
        $form = $this->factory->create();
        //$form->addText('from', null)->setHtmlType("time")->setRequired(true);
        //$form->addText('to', null)->setHtmlType("time")->setRequired(true);
        $form->addHidden("date");

        $form->addSelect('office', null, $this->officeManager->getActive()->fetchPairs('id', 'name'))
            ->setRequired(true);

        $form->addSelect('shift_type', null, $this->shiftTypeManager->getActive()->order('start')->fetchPairs('id', 'name'))
            ->setRequired(true);

        $form->addSelect('shift_version', null, $this->shiftVersionManager->getTable()->fetchPairs('id', 'name'))->setRequired(true);

        $form->addSubmit('submit', null);

        $form->onSuccess[] = function (Form $form, \stdClass $values) use ($onSuccess): void {
            $shiftType = $this->shiftTypeManager->get($values->shift_type);
            $from = Nette\Utils\DateTime::from($values->date . ' ' . $shiftType->start->format('H:i'));
            $to = Nette\Utils\DateTime::from($values->date . ' ' . $shiftType->end->format('H:i'));

            if($to <= $from) {
                $form['to']->addError('Směna nemůže skončit dřív než začne!');
                return;
            }
            if($from->format('Gi') < $shiftType->start->format('Gi')) {
                $form['from']->addError('Tento čas není podporovaný typem směny!');
                return;
            }
            if($to->format('Gi') > $shiftType->end->format('Gi')) {
                $form['to']->addError('Tento čas není podporovaný typem směny!');
                return;
            }

            if($this->shiftManager->checkDupe(Nette\Utils\DateTime::from($values->date), $values->shift_type, $values->office)) {
                $form['shift_type']->addError('Duplicitní typ směny ve stejný den ve stejné ordinaci!');
                return;
            }

            try {
                $this->shiftManager->add($from, $to, $values->office, $values->shift_type, $values->shift_version);
            } catch (ShiftOverlappingException $e) {
                $form['office']->addError('Směna v této ordinace se překrývá s již existující!');
                return;
            }
            $onSuccess(Nette\Utils\DateTime::from($values->date));
        };

        return $form;
    }

}