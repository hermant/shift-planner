<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\ShiftTypeOverlappingException;
use Nette;
use App\Model\DuplicateNameException;
use App\Model\ShiftTypeManager;
use Nette\Application\UI\Form;

class ShiftTypeCreateFactory
{
    /** @var FormFactory */
    private $factory;

    /** @var ShiftTypeManager */
    private $shiftTypeManager;


    public function __construct(FormFactory $factory, ShiftTypeManager $shiftTypeManager)
    {
        $this->factory = $factory;
        $this->shiftTypeManager = $shiftTypeManager;
    }

    public function create(callable $onSuccess): Form
    {
        $form = $this->factory->create();
        $form->addText('name')
            ->setRequired();

        $form->addText('from', null)->setHtmlType("time")->setRequired(true);
        $form->addText('to', null)->setHtmlType("time")->setRequired(true);

        $form->addSubmit('submit');

        $form->onSuccess[] = function (Form $form, \stdClass $values) use ($onSuccess): void {
            try {
                $from = Nette\Utils\DateTime::from(Nette\Utils\DateTime::from(0)->format('Y-m-d') . ' ' . $values->from);
                $to = Nette\Utils\DateTime::from(Nette\Utils\DateTime::from(0)->format('Y-m-d') . ' ' . $values->to);
                if($to <= $from) {
                    $form['to']->addError('Směna nemůže skončit dřív než začne!');
                    return;
                }

                $this->shiftTypeManager->add($values->name, Nette\Utils\DateTime::from($values->from), Nette\Utils\DateTime::from($values->to));
            } catch (DuplicateNameException $e) {
                $form['name']->addError('Již existuje typ směny s tímto názvem.');
                return;
            } catch (ShiftTypeOverlappingException $e) {
                $form['from']->addError('Typy směn se nesmí překrývat s ostatními.');
                $form['to']->addError('Typy směn se nesmí překrývat s ostatními.');
                return;
            }
            $onSuccess();
        };

        return $form;
    }
}