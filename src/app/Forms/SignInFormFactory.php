<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\DeactivatedAccountException;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;


final class SignInFormFactory
{
	use Nette\SmartObject;

	/** @var FormFactory */
	private $factory;

	/** @var User */
	private $user;


	public function __construct(FormFactory $factory, User $user)
	{
		$this->factory = $factory;
		$this->user = $user;
	}


	public function create(callable $onSuccess): Form
	{
		$form = $this->factory->create();
		$form->addText('email', null)
			->setRequired();

		$form->addPassword('password', null)
			->setRequired();

		$form->addCheckbox('remember', null);

		$form->addSubmit('submit', null);

		$form->onSuccess[] = function (Form $form, \stdClass $values) use ($onSuccess): void {
			try {
				$this->user->setExpiration($values->remember ? '14 days' : '20 minutes');
				$this->user->login($values->email, $values->password);
			} catch (Nette\Security\AuthenticationException $e) {
				$form['password']->addError('Kombinace zadaného emailu a hesla neexistuje.');
				return;
			} catch (DeactivatedAccountException $€) {
                $form['email']->addError('Účet je deaktivovaný.');
                return;
            }
			$onSuccess();
		};

		return $form;
	}
}
