<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\ShiftMonthLimitManager;
use App\Model\ShiftTypeManager;
use App\Model\UserRoleManager;
use Nette;
use App\Model\UserManager;
use Nette\Application\UI\Form;

class UserEditFactory
{
    /** @var FormFactory */
    private $factory;

    /** @var UserManager */
    private $userManager;

    /** @var UserRoleManager */
    private $userRoleManager;

    /** @var ShiftTypeManager */
    private $shiftTypeManager;

    /** @var ShiftMonthLimitManager */
    private $shiftMonthLimitManager;


    public function __construct(FormFactory $factory, UserManager $userManager, ShiftTypeManager $shiftTypeManager, UserRoleManager $userRoleManager, ShiftMonthLimitManager $shiftMonthLimitManager)
    {
        $this->factory = $factory;
        $this->userManager = $userManager;
        $this->userRoleManager = $userRoleManager;
        $this->shiftTypeManager = $shiftTypeManager;
        $this->shiftMonthLimitManager = $shiftMonthLimitManager;
    }

    public function create(int $id, callable $onSuccess): Form
    {
        $form = $this->factory->create();
        $form->addEmail('email')
            ->setRequired();

        $form->addPassword('password')
            ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 8)
            ->addRule(Form::PATTERN, 'Musí obsahovat alespoň 1 číslici', '.*[0-9].*')
            ->addRule(Form::PATTERN, 'Musí obsahovat alespoň 1 velké písmeno', '.*[A-Z].*')
            ->addRule(Form::PATTERN, 'Musí obsahovat alespoň 1 malé písmeno', '.*[a-z].*');

        $form->addSelect('user_role_id', null, $this->userRoleManager->getTable()->fetchPairs('id', 'display_name'))
            ->setRequired();

        $form->addText('firstname')
            ->setRequired();

        $form->addText('lastname')
            ->setRequired();

        $form->addText('titles_before');

        $form->addText('titles_after');

        foreach($this->shiftTypeManager->getActive() as $type) {
            $form->addInteger('limit' . (string)$type->id)->setRequired();
            //$form->addCheckbox('check' . (string)$type->id);
        }

        $form->addSubmit('submit');

        $form->onSuccess[] = function (Form $form, \stdClass $values) use ($id, $onSuccess): void {
            $this->userManager->update($id, $values->email, $values->password, $values->firstname, $values->lastname, $values->titles_before, $values->titles_after, $values->user_role_id);
            foreach($this->shiftTypeManager->getActive() as $type) {
                $this->shiftMonthLimitManager->update($id, (int)$type->id, ((array)$values)['limit' . (string)$type->id]);
            }
            $onSuccess();
        };

        return $form;
    }
}