<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\DuplicateNameException;
use App\Model\OfficeManager;
use Nette;
use Nette\Application\UI\Form;

class OfficeEditFactory
{
    /** @var FormFactory */
    private $factory;

    /** @var OfficeManager */
    private $officeManager;


    public function __construct(FormFactory $factory, OfficeManager $officeManager)
    {
        $this->factory = $factory;
        $this->officeManager = $officeManager;
    }

    public function create(int $id, callable $onSuccess): Form
    {
        $form = $this->factory->create();
        $form->addText('name')
            ->setRequired();

        $form->addSubmit('submit');

        $form->onSuccess[] = function (Form $form, \stdClass $values) use ($id, $onSuccess): void {
            try {
                $this->officeManager->update($id, $values->name);
            } catch (DuplicateNameException $e) {
                $form['name']->addError('Již existuje ordinace s tímto názvem.');
                return;
            }
            $onSuccess();
        };

        return $form;
    }
}