<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\OfficeManager;
use App\Model\ShiftManager;
use App\Model\ShiftOverlappingException;
use App\Model\ShiftTypeManager;
use App\Model\ShiftVersionManager;
use App\Model\UserManager;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;

class ShiftEditFormFactory
{

    use Nette\SmartObject;

    /** @var FormFactory */
    private $factory;

    /** @var ShiftManager */
    private $shiftManager;

    /** @var OfficeManager */
    private $officeManager;

    /** @var ShiftTypeManager */
    private $shiftTypeManager;

    /** @var UserManager */
    private $userManager;

    /** @var ShiftVersionManager */
    private $shiftVersionManager;

    public function __construct(FormFactory $factory, ShiftManager $shiftManager, OfficeManager $officeManager, ShiftTypeManager $shiftTypeManager, UserManager $userManager, ShiftVersionManager  $shiftVersionManager)
    {
        $this->factory = $factory;
        $this->shiftManager = $shiftManager;
        $this->officeManager = $officeManager;
        $this->shiftTypeManager = $shiftTypeManager;
        $this->userManager = $userManager;
        $this->shiftVersionManager = $shiftVersionManager;
    }

    public function create(int $id, callable $onSuccess): Form
    {
        $form = $this->factory->create();
        $form->addText('from', null)->setHtmlType("time")->setRequired(true);
        $form->addText('to', null)->setHtmlType("time")->setRequired(true);
        $form->addHidden("date");

        $form->addSelect('office', null, $this->officeManager->getActive()->fetchPairs('id', 'name'))
            ->setRequired(true)->setDisabled();

        $form->addSelect('shift_type', null, $this->shiftTypeManager->getActive()->order('start')->fetchPairs('id', 'name'))
            ->setRequired(true)->setDisabled();

        $form->addSelect('shift_version', null, $this->shiftVersionManager->getTable()->fetchPairs('id', 'name'))
            ->setRequired(true)->setDisabled();

        $doctors = $this->userManager->getDoctors();
        $doctorsArray = array();
        foreach($doctors as $doctor) {
            $doctorsArray[$doctor->id] = $this->userManager->getFullName($doctor);
        }

        $nurses = $this->userManager->getNurses();
        $nursesArray = array();
        foreach($nurses as $nurse) {
            $nursesArray[$nurse->id] = $this->userManager->getFullName($nurse);
        }

        $receptionists = $this->userManager->getReceptionists();
        $receptionistsArray = array();
        foreach($receptionists as $rec) {
            $receptionistsArray[$rec->id] = $this->userManager->getFullName($rec);
        }

        $form->addSelect('doctor', null, $doctorsArray)->setPrompt('------');

        $form->addSelect('nurse', null, $nursesArray)->setPrompt('------');

        $form->addSelect('receptionist', null, $receptionistsArray)->setPrompt('------');

        $form->addSubmit('submit', null);

        $form->onSuccess[] = function (Form $form, \stdClass $values) use ($id, $onSuccess): void {
            $from = Nette\Utils\DateTime::from($values->date . ' ' . $values->from);
            $to = Nette\Utils\DateTime::from($values->date . ' ' . $values->to);

            if($to <= $from) {
                $form['to']->addError('Směna nemůže skončit dřív než začne!');
                return;
            }
            $shiftType = $this->shiftTypeManager->get((int)$this->shiftManager->get($id)->shift_type_id);
            if($from->format('Gi') < $shiftType->start->format('Gi')) {
                $form['from']->addError('Tento čas není podporovaný typem směny!');
                return;
            }
            if($to->format('Gi') > $shiftType->end->format('Gi')) {
                $form['to']->addError('Tento čas není podporovaný typem směny!');
                return;
            }

            $this->shiftManager->update($id, $from, $to, $values->doctor, $values->nurse, $values->receptionist);
            $onSuccess(Nette\Utils\DateTime::from($values->date));
        };

        return $form;
    }

}