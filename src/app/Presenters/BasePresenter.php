<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    protected function startup() {
        parent::startup();
        if (!$this->getUser()->isAllowed($this->getName(), $this->getAction())) {
            $this->redirect("Sign:in", ['backlink' => $this->storeRequest()]);
            //throw new Nette\Application\ForbiddenRequestException;
        }
    }

    protected function beforeRender()
    {
        $this->template->presenter = $this->getName();
        $this->template->action = $this->getAction();
    }
}
