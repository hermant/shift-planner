<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Forms\OfficeCreateFactory;
use App\Forms\OfficeEditFactory;
use App\Forms\UserCreateFactory;
use App\Forms\UserEditFactory;
use App\Model\OfficeManager;
use App\Model\OfficeNotFoundException;
use App\Model\UserManager;
use Nette;
use Nette\Application\UI\Form;

class OfficePresenter extends BasePresenter
{
    /** @var OfficeManager */
    private $officeManager;

    /** @var OfficeCreateFactory */
    private $officeCreateFactory;

    /** @var OfficeEditFactory */
    private $officeEditFactory;

    public function __construct(OfficeManager $officeManager, OfficeCreateFactory $officeCreateFactory, OfficeEditFactory $officeEditFactory)
    {
        $this->officeManager = $officeManager;
        $this->officeCreateFactory = $officeCreateFactory;
        $this->officeEditFactory = $officeEditFactory;
    }

    public function renderDefault() {
        $this->template->offices = $this->officeManager->getTable()->order('active DESC, name');
    }

    public function handleDeactivate(string $id) {
        try {
            $this->officeManager->deactivate(intval($id));
            if($this->isAjax()) {
                $this->redrawControl('officesContainer');
            }
        } catch (OfficeNotFoundException $e) {

        }
    }

    public function renderEdit(string $id) {
        $office = $this->officeManager->get(intval($id));
        if (!$office) {
            $this->error('Ordinace nebyla nalezena!');
        }
        $this['officeEditForm']->setDefaults($office->toArray());
    }


    protected function createComponentOfficeCreateForm(): Form
    {
        return $this->officeCreateFactory->create(function (): void {
            $this->redirect('Office:');
        });
    }

    protected function createComponentOfficeEditForm(): Form
    {
        $id = $this->getParameter('id');
        return $this->officeEditFactory->create(intval($id), function (): void {
            $this->redirect('Office:');
        });
    }
}