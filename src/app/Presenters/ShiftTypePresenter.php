<?php

declare(strict_types=1);

namespace App\Presenters;


use App\Forms\ShiftTypeCreateFactory;
use App\Forms\ShiftTypeEditFactory;
use App\Model\ShiftTypeManager;
use App\Model\ShiftTypeNotFoundException;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;

class ShiftTypePresenter extends BasePresenter
{
    /** @var ShiftTypeManager */
    private $shiftTypeManager;

    /** @var ShiftTypeCreateFactory */
    private $shiftTypeCreateFactory;

    /** @var ShiftTypeEditFactory */
    private $shiftTypeEditFactory;

    public function __construct(ShiftTypeManager $shiftTypeManager, ShiftTypeCreateFactory $shiftTypeCreateFactory, ShiftTypeEditFactory $shiftTypeEditFactory)
    {
        $this->shiftTypeManager = $shiftTypeManager;
        $this->shiftTypeCreateFactory = $shiftTypeCreateFactory;
        $this->shiftTypeEditFactory = $shiftTypeEditFactory;
    }

    public function renderDefault() {
        $this->template->types = $this->shiftTypeManager->getActive()->order('HOUR(start), MINUTE(start), name');
    }

    public function renderEdit(string $id) {
        $type = $this->shiftTypeManager->get(intval($id));
        if (!$type) {
            $this->error('Typ směny nebyl nalezen!');
        }
        $data = $type->toArray();
        $data['from'] = DateTime::from($data['start'])->format('H:i');
        $data['to'] = DateTime::from($data['end'])->format('H:i');
        $this['shiftTypeEditForm']->setDefaults($data);
    }

    public function handleDeactivate(string $id) {
        try {
            $this->shiftTypeManager->deactivate(intval($id));
            if($this->isAjax()) {
                $this->redrawControl('shiftTypesContainer');
            }
        } catch (ShiftTypeNotFoundException $e) {

        }
    }

    protected function createComponentShiftTypeCreateForm(): Form
    {
        return $this->shiftTypeCreateFactory->create(function (): void {
            $this->redirect('ShiftType:');
        });
    }

    protected function createComponentShiftTypeEditForm(): Form
    {
        $id = $this->getParameter('id');
        return $this->shiftTypeEditFactory->create(intval($id), function (): void {
            $this->redirect('ShiftType:');
        });
    }
}