<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\OfficeManager;
use App\Model\ShiftManager;
use App\Model\ShiftMonthLimitManager;
use App\Model\ShiftNotFoundException;
use App\Model\ShiftTypeManager;
use App\Model\ShiftVersionManager;
use App\Model\UserManager;
use DateTimeImmutable;
use Nette\Application\Responses\JsonResponse;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\DateTime;
use App\Forms;

class ShiftPresenter extends BasePresenter
{
    /** @var Forms\ShiftCreateFormFactory */
    private $shiftCreateFactory;

    /** @var Forms\ShiftEditFormFactory */
    private $shiftEditFactory;

    /** @var ShiftManager */
    private $shiftManager;

    /** @var ShiftTypeManager */
    private $shiftTypeManager;

    /** @var UserManager */
    private $userManager;

    /** @var ShiftMonthLimitManager */
    private $shiftMonthLimitManager;

    /** @var OfficeManager */
    private $officeManager;

    public function __construct(ShiftManager $shiftManager, ShiftTypeManager $shiftTypeManager, UserManager $userManager, Forms\ShiftCreateFormFactory $shiftCreateFactory, ShiftMonthLimitManager $shiftMonthLimitManager, OfficeManager $officeManager, Forms\ShiftEditFormFactory $shiftEditFactory)
    {
        $this->shiftManager = $shiftManager;
        $this->userManager = $userManager;
        $this->shiftTypeManager = $shiftTypeManager;
        $this->shiftCreateFactory = $shiftCreateFactory;
        $this->shiftMonthLimitManager = $shiftMonthLimitManager;
        $this->officeManager = $officeManager;
        $this->shiftEditFactory = $shiftEditFactory;
    }

    public function renderCreate(?string $id) {
        if(!isset($id)) {
            $this->template->date = DateTime::from(0);
        } else {
            $this->template->date = DateTime::from($id);
        }
        $this['shiftCreateForm']->setDefaults(
            array('date' => $this->template->date->format('Y-m-d'))
        );
    }

    public function renderEdit(string $id) {
        $shift = $this->shiftManager->get(intval($id));
        $this->template->shift = $shift;
        $this->template->date = $shift->working_hours_from;

        $data = $shift->toArray();

        $data['from'] = DateTime::from($data['working_hours_from'])->format('H:i');
        $data['to'] = DateTime::from($data['working_hours_to'])->format('H:i');
        $data['date'] = $this->template->date->format('Y-m-d');
        $data['shift_type'] = $data['shift_type_id'];
        $data['shift_version'] = $data['shift_version_id'];

        $this['shiftEditForm']->setDefaults($data);
    }

    public function renderViewOld(string $id) {
        $this->template->shift = $this->shiftManager->get(intval($id));

        $this->template->addFilter('fullname', function (?int $userId) {
            if($userId == null) {
                return '';
            } else {
                return $this->userManager->getFullName($this->userManager->get($userId));
            }
        });

        $this->template->canSignOut = $this->canSignOut($this->template->shift);
    }

    public function renderView(string $id, string $shiftType) {
        $this->template->date = DateTime::from($id);
        $this->template->day = DateTime::from($id)->format('d. m. Y');
        $this->template->shiftType = $this->shiftTypeManager->get(intval($shiftType))->name;
        $this->template->shifts = $this->shiftManager->getAllOnDay(DateTime::from($id), intval($shiftType));
        $this->template->canSignOut = array();
        $this->template->canSignIn = array();

        foreach($this->template->shifts as $shift) {
            $this->template->canSignOut[$shift->id] = $this->canSignOut($shift) == 0;
            $this->template->canSignIn[$shift->id] = $this->canSignIn($shift) == 0;
        }

        $this->template->addFilter('fullname', function (?int $userId) {
            if($userId == null) {
                return '';
            } else {
                return $this->userManager->getFullName($this->userManager->get($userId));
            }
        });


    }

    public function renderListview(?string $id) {
        if($id == null) {
            $first = new DateTime();
        } else {
            $first = new DateTime($id);
        }
        $first->modify('monday this week');
        $last = clone $first->modify('sunday this week');

        $this->template->shiftTypes = $this->shiftTypeManager->getActive()->order('start');
        $this->template->offices = $this->officeManager->getActive()->order('name');

    }

    public function actionDelete(int $id) {
        $this->shiftManager->get($id)->delete();
        $this->redirect('Homepage:');
    }

    private function canSignOut(ActiveRow $shift) : int {
        if($shift->doctor != $this->getUser()->getIdentity()->getId() && $shift->nurse != $this->getUser()->getIdentity()->getId() && $shift->receptionist != $this->getUser()->getIdentity()->getId()) {
            return 1;
        }

        $deadline = clone $shift->working_hours_from;
        $deadline->modify('last day of previous month');
        $deadline->modify('-3 days');
        $deadline->setTime(23, 59, 59);
        if(DateTime::from(0) >= $deadline) {
            return 2;
        }

        return 0;
    }

    private function canSignIn(ActiveRow $shift) : int {
        //$currentTime = $this->shiftManager->getMonthWorkingHours($this->user->identity->getId(), (int)$shift->shift_type->id, $shift->working_hours_from);
        //$optimalTime = $this->shiftMonthLimitManager->getOptimalMonthWorkingHoursPerType(intval($shift->working_hours_from->format('Y')), intval($shift->working_hours_from->format('m')), (int)$shift->shift_type->id, $this->getUser()->getIdentity()->getId());

        $currentDays = $this->shiftManager->getMonthWorkingDays($this->user->identity->getId(), (int)$shift->shift_type->id, $shift->working_hours_from);
        $optimalDays = $this->shiftMonthLimitManager->getOptimalMonthWorkingDaysPerType(intval($shift->working_hours_from->format('Y')), intval($shift->working_hours_from->format('m')), (int)$shift->shift_type->id, $this->getUser()->getIdentity()->getId());

        if($currentDays >= $optimalDays) {
            return 1;
        }

        $deadline = clone $shift->working_hours_from;
        $deadline->modify('last day of previous month');
        $deadline->modify('-3 days');
        $deadline->setTime(23, 59, 59);
        if(DateTime::from(0) >= $deadline) {
            return 2;
        }

        if($this->shiftManager->checkMultiple($shift->working_hours_from, $this->getUser()->getId())) {
            return 3;
        }

        /*foreach($this->shiftTypeManager->getTable() as $type) {
            if($this->shiftManager->getFridayShiftsCount($this->getUser()->getId(), (int)$type->id, $shift->working_hours_from) >= $this->shiftMonthLimitManager->getOptimalFridayShiftsPerType(intval($shift->working_hours_from->format('Y')), intval($shift->working_hours_from->format('m')))) {
                return 4;
            }
        }*/

        if($shift->working_hours_from->format('w') == 5) {
            if($this->shiftManager->getFridayShiftsCount($this->getUser()->getId(), (int)$shift->shift_type_id, $shift->working_hours_from) >= $this->shiftMonthLimitManager->getOptimalFridayShiftsPerType(intval($shift->working_hours_from->format('Y')), intval($shift->working_hours_from->format('m')))) {
                return 4;
            }
        }

        return 0;
    }

    private function canSignInType(int $shiftTypeId, DateTime $date) : int {
        //$currentTime = $this->shiftManager->getMonthWorkingHours($this->user->identity->getId(), (int)$shift->shift_type->id, $shift->working_hours_from);
        //$optimalTime = $this->shiftMonthLimitManager->getOptimalMonthWorkingHoursPerType(intval($shift->working_hours_from->format('Y')), intval($shift->working_hours_from->format('m')), (int)$shift->shift_type->id, $this->getUser()->getIdentity()->getId());

        $currentDays = $this->shiftManager->getMonthWorkingDays($this->user->identity->getId(), $shiftTypeId, $date);
        $optimalDays = $this->shiftMonthLimitManager->getOptimalMonthWorkingDaysPerType(intval($date->format('Y')), intval($date->format('m')), $shiftTypeId, $this->getUser()->getIdentity()->getId());

        if($currentDays >= $optimalDays) {
            return 1;
        }

        $deadline = clone $date;
        $deadline->modify('last day of previous month');
        $deadline->modify('-3 days');
        $deadline->setTime(23, 59, 59);
        if(DateTime::from(0) >= $deadline) {
            return 2;
        }

        if($this->shiftManager->checkMultiple($date, $this->getUser()->getId())) {
            return 3;
        }

        /*foreach($this->shiftTypeManager->getTable() as $type) {
            if($this->shiftManager->getFridayShiftsCount($this->getUser()->getId(), (int)$type->id, $shift->working_hours_from) >= $this->shiftMonthLimitManager->getOptimalFridayShiftsPerType(intval($shift->working_hours_from->format('Y')), intval($shift->working_hours_from->format('m')))) {
                return 4;
            }
        }*/

        if($date->format('w') == 5) {
            if($this->shiftManager->getFridayShiftsCount($this->getUser()->getId(), (int)$shiftTypeId, $date) >= $this->shiftMonthLimitManager->getOptimalFridayShiftsPerType(intval($date->format('Y')), intval($date->format('m')))) {
                if(!$this->shiftManager->isOtherFridaysFullDoctor((int)$shiftTypeId, $date))
                {
                    return 4;
                }

            }
        }

        return 0;
    }


    public function handleSign(int $shiftId) {
        try {
            if($this->user->roles[0] == 'doctor') {
                $this->shiftManager->signDoctor($shiftId, $this->user->getIdentity()->getId());
            }
            if($this->user->roles[0] == 'nurse') {
                $this->shiftManager->signNurse($shiftId, $this->user->getIdentity()->getId());
            }
        } catch (ShiftNotFoundException $e) {
        }
        if($this->isAjax()) {
            $this->redrawControl('shiftsContainer');
        }
    }

    public function handleSignOut(int $shiftId) {
        try {
            $this->signOut($shiftId);
        } catch (ShiftNotFoundException $e) {
        }
        if($this->isAjax()) {
            $this->redrawControl('shiftsContainer');
        }
    }

    /**
     * @param int $shiftId
     * @throws ShiftNotFoundException
     */
    public function signOut(int $shiftId) {
        if($this->user->roles[0] == 'doctor') {
            $this->shiftManager->signDoctor($shiftId, null);
        }
        if($this->user->roles[0] == 'nurse') {
            $this->shiftManager->signNurse($shiftId, null);
        }
        if($this->user->roles[0] == 'receptionist') {
            $this->shiftManager->signReceptionist($shiftId, null);
        }
    }

    public function handleDeleteDoctor(int $shiftId) {
        $this->shiftManager->deleteDoctor($shiftId);
    }

    public function handleDeleteNurse(int $shiftId) {
        $this->shiftManager->deleteNurse($shiftId);
    }

    public function handleDeleteReceptionist(int $shiftId) {
        $this->shiftManager->deleteReceptionist($shiftId);
    }

    protected function createComponentShiftCreateForm(): Form
    {
        return $this->shiftCreateFactory->create(function (DateTime $date): void {
            $this->redirect('Homepage:default', $date->format('Y-m-d'));
        });
    }

    protected function createComponentShiftEditForm(): Form
    {
        $id = $this->getParameter('id');
        return $this->shiftEditFactory->create(intval($id), function (DateTime $date) use ($id): void {
            $shift = $this->shiftManager->get(intval($id));
            $this->redirect('Shift:view', $shift->working_hours_from->format('Y-m-d'), $shift->shift_type_id);
        });
    }

    public function actionOldInterval($start, $end) {
        $data = array();
        $from = DateTime::from($start);
        $to = DateTime::from($end);
        foreach($this->shiftManager->getAll($from, $to) as $id => $row) {
            $color = 'green';
            if($this->getUser()->isInRole('doctor')) {
                if($row->doctor) {
                    if($row->doctor == $this->getUser()->getIdentity()->getId()) {
                        if($row->nurse) {
                            $color = '#181818';
                        } else {
                            $color = 'gray';
                        }
                    } else {
                        $color = 'red';
                    }
                } else {
                    if($row->nurse) {
                        $color = 'blue';
                    }
                }
            } else if($this->getUser()->isInRole('nurse')) {
                if($row->nurse) {
                    if($row->nurse == $this->getUser()->getIdentity()->getId()) {
                        if($row->doctor) {
                            $color = '#181818';
                        } else {
                            $color = 'gray';
                        }
                    } else {
                        $color = 'red';
                    }
                }
            } else if($this->getUser()->isInRole('admin')) {
                if($row->doctor || $row->nurse) {
                    if($row->doctor && $row->nurse) {
                        $color = 'red';
                    } else {
                        $color = 'blue';
                    }
                }
            }

            $data[] = array(
                'title' => $row->office->name,
                'start' => $row->working_hours_from,
                'end' => $row->working_hours_to,
                'url' => $this->link('Shift:viewOld', $id),
                'color' => $color
            );
        }

        $this->sendResponse( new JsonResponse( $data ));
    }

    public function actionInterval($start, $end) {
        $data = array();
        $from = DateTime::from($start);
        $to = DateTime::from($end);

        if(!$this->getUser()->isInRole('admin')) {
            $today = DateTime::from(0);
            if($from >= $today) {
                if(intval($today->format('d')) < 10)
                {
                    $this->sendResponse( new JsonResponse( $data ));
                } else {
                    if(intval($from->format('m')) > (intval($today->format('m')) + 1)) {
                        $this->sendResponse( new JsonResponse( $data ));
                    }
                }
            }
        }


        foreach($this->shiftManager->getAllGroupedByType($from, $to) as $id => $row) {
            $color = 'green';

            if($this->getUser()->isInRole('doctor')) {
                if($this->shiftManager->isFullDoctors($row->working_hours_from, (int)$row->shift_type_id)) {
                    $color = 'red';
                }
                if($this->shiftManager->isSigned($row->working_hours_from, (int)$row->shift_type_id, $this->getUser()->getId())) {
                    $color = 'blue';
                }
            } else if($this->getUser()->isInRole('nurse')) {
                if($this->shiftManager->isFullNurses($row->working_hours_from, (int)$row->shift_type_id)) {
                    $color = 'red';
                }
                if($this->shiftManager->isSigned($row->working_hours_from, (int)$row->shift_type_id, $this->getUser()->getId())) {
                    $color = 'blue';
                }
            } else if($this->getUser()->isInRole('receptionist')) {
                if($this->shiftManager->isFullReceptionists($row->working_hours_from, (int)$row->shift_type_id)) {
                    $color = 'red';
                }
                if($this->shiftManager->isSigned($row->working_hours_from, (int)$row->shift_type_id, $this->getUser()->getId())) {
                    $color = 'blue';
                }
            } else if($this->getUser()->isInRole('admin')) {
                if($this->shiftManager->isFullAll($row->working_hours_from, (int)$row->shift_type_id)) {
                    $color = 'red';
                }
            }

            $arr = array(
                'title' => $row->shift_type->name,
                'start' => $row->working_hours_from,
                'end' => $row->working_hours_to,
                //'url' => $this->link('Shift:view', $row->working_hours_from->format('Y-m-d'), $row->shift_type_id),
                'color' => $color,
                'shiftTypeId' => $row->shift_type_id
            );

            if($this->getUser()->isInRole('admin')) {
                $arr['url'] = $this->link('Shift:view', $row->working_hours_from->format('Y-m-d'), $row->shift_type_id);
            }
            $data[] = $arr;
        }

        $this->sendResponse( new JsonResponse( $data ));
    }

    public function actionHolidays($start, $end) {
        $data = array();
        $from = DateTime::from($start);
        $to = DateTime::from($end);

        $publicHolidays = array(
            '1.1.',
            '1.5.',
            '8.5.',
            '5.7.',
            '6.7.',
            '28.9.',
            '28.10.',
            '17.11.',
            '24.12.',
            '25.12.',
            '26.12.'
        );

        $easterDays = easter_days(intval($from->format('Y')));

        $easterSunday = new DateTimeImmutable('21.3.' . $from->format('Y'));
        $easterSunday = $easterSunday->modify('+' . $easterDays . ' days');

        $easterMonday = $easterSunday->modify('+1 day');
        $publicHolidays[] = $easterMonday->format('j.n.');

        $goodFriday = $easterSunday->modify('friday last week');
        $publicHolidays[] = $goodFriday->format('j.n.');

        foreach($publicHolidays as $val) {
            $date = DateTime::fromParts(intval($from->format('Y')), intval(explode(".", $val)[1]), intval(explode(".", $val)[0]));

            $arr = array(
                'start' => $date->format('Y-m-d'),
                'end' => $date->format('Y-m-d'),
                'rendering' => 'background',
            );

            $data[] = $arr;
        }

        $this->sendResponse( new JsonResponse( $data ));
    }

    public function actionSignInOut($shiftTypeId, $date) {

        $data = array();

        if($this->shiftManager->isSigned(DateTime::from($date), intval($shiftTypeId), $this->getUser()->id)) {
            $shift = $this->shiftManager->getSigned(DateTime::from($date), intval($shiftTypeId), $this->getUser()->id);
            $data['result'] = $this->canSignOut($shift) + 10;

            if($data['result'] == 10) {
                $this->signOut(intval($shift->id));
            }
        } else {
            $data['result'] = $this->canSignInType(intval($shiftTypeId), DateTime::from($date));

            if($data['result'] == 0) {
                try {
                    if($this->getUser()->isInRole('doctor')) {
                        $this->shiftManager->signDoctorType(DateTime::from($date), intval($shiftTypeId), $this->getUser()->id);
                    } else if($this->getUser()->isInRole('nurse')) {
                        $this->shiftManager->signNurseType(DateTime::from($date), intval($shiftTypeId), $this->getUser()->id);
                    } else if($this->getUser()->isInRole('receptionist')) {
                        $this->shiftManager->signReceptionistType(DateTime::from($date), intval($shiftTypeId), $this->getUser()->id);
                    }
                } catch (ShiftNotFoundException $e) {
                    $data['result'] = 5;
                }
            }
        }


        $this->sendResponse( new JsonResponse( $data ));
    }



}