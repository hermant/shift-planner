<?php

declare(strict_types=1);

namespace App\Presenters;


use App\Model\OfficeManager;
use App\Model\ShiftManager;
use App\Model\ShiftMonthLimitManager;
use App\Model\ShiftTypeManager;
use App\Model\UserManager;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\DateTime;

use Mpdf\Mpdf as mPDF;
use Nette\Application\UI\ITemplateFactory;

final class HomepagePresenter extends BasePresenter
{
    /** @var ITemplateFactory @inject */
    public $templateFactory;

    /** @var ShiftTypeManager */
    private $shiftTypeManager;

    /** @var ShiftManager */
    private $shiftManager;

    /** @var OfficeManager */
    private $officeManager;

    /** @var UserManager */
    private $userManager;

    /** @var ShiftMonthLimitManager */
    private $shiftMonthLimitManager;

    public function __construct(ShiftTypeManager $shiftTypeManager, ShiftManager $shiftManager, ShiftMonthLimitManager $shiftMonthLimitManager, OfficeManager $officeManager, UserManager $userManager)
    {
        $this->shiftTypeManager = $shiftTypeManager;
        $this->shiftManager = $shiftManager;
        $this->shiftMonthLimitManager = $shiftMonthLimitManager;
        $this->officeManager = $officeManager;
        $this->userManager = $userManager;
    }

	public function renderDefault(?string $id): void
	{
        $this->template->date = is_null($id) ? DateTime::from(0) : DateTime::from($id);

        $deadline = DateTime::from(0);
        $deadline->modify('last day of this month');
        $deadline->modify('-3 days');
        $deadline->setTime(23, 59, 59);
        $this->template->deadline = $deadline;

        $this->template->visibility = date_create(DateTime::from(0)->format('Y-m-10'));


	}

	public function renderPreview(?string $id) {
        $this->template->offices = $this->officeManager->getActive()->order('name');
        $this->template->shiftTypes = $this->shiftTypeManager->getActive()->order('start');
        $this->template->dates = array();
        $date = new DateTime('now');
        $date->modify('first day of previous month');
        $this->template->dates[] = clone $date;
        $date->modify('first day of next month');
        $this->template->dates[] = clone $date;
        $date->modify('first day of next month');
        $this->template->dates[] = clone $date;

        $this->template->addFilter('current', function (int $shiftTypeId, \DateTime $date) {
            return $this->shiftManager->getMonthWorkingDays($this->user->identity->getId(), $shiftTypeId, $date);
        });

        $this->template->addFilter('optimal', function (int $shiftTypeId, \DateTime $date) {
            return $this->shiftMonthLimitManager->getOptimalMonthWorkingDaysPerType(intval($date->format('Y')), intval($date->format('m')), $shiftTypeId, $this->getUser()->getIdentity()->getId());
        });

        if(!isset($this->template->date)) {
            $this->kek(DateTime::from($id == null ? new DateTime() : new DateTime($id)));
        }
    }

    public function handleNextWeek(string $id) {
        $date = DateTime::from($id);
        $date->modify('monday next week');
        $this->kek($date);

        if($this->isAjax()) {
            $this->redrawControl('shiftsContainer');
        }
    }

    public function handlePrevWeek(string $id) {
        $date = DateTime::from($id);
        $date->modify('monday previous week');
        $this->kek($date);

        if($this->isAjax()) {
            $this->redrawControl('shiftsContainer');
        }
    }

    public function kek(DateTime $date) {
        $this->template->date = $date;
        $first = clone $date;
        $first->modify('monday this week');
        $first->setTime(0, 0, 0);
        $last = clone $first;
        $last->modify('sunday this week');
        $last->setTime(23, 59, 59);

        $this->template->shifts = array();
        $shifts = $this->shiftManager->getAllVisible($first, $last);
        foreach($shifts as $shift) {
            $weekday = $shift->working_hours_from->format('w');
            $this->template->shifts[$weekday][$shift->office_id][$shift->shift_type_id]['doctor'] = ($shift->doctor == null ? null : $this->userManager->getFullName($this->userManager->get(intval($shift->doctor))));
            $this->template->shifts[$weekday][$shift->office_id][$shift->shift_type_id]['nurse'] = ($shift->nurse == null ? null : $this->userManager->getFullName($this->userManager->get(intval($shift->nurse))));
            $this->template->shifts[$weekday][$shift->office_id][$shift->shift_type_id]['receptionist'] = ($shift->receptionist == null ? null : $this->userManager->getFullName($this->userManager->get(intval($shift->receptionist))));
            $this->template->shifts[$weekday][$shift->office_id][$shift->shift_type_id]['start'] = $shift->working_hours_from->format('H:i');
            $this->template->shifts[$weekday][$shift->office_id][$shift->shift_type_id]['end'] = $shift->working_hours_to->format('H:i');
        }
        $this->template->first = $first;
        $this->template->last = $last;

        $now = DateTime::from(0);
        $now->modify('+3 days');
        $tmp = clone $date;
        $tmp->modify('monday next week');
        $this->template->isVisible = $tmp->format('Y') <= $now->format('Y') && $tmp->format('m') <= $now->format('m');
    }

    public function handleExportPdf(?string $id)
    {
        $this->template->offices = $this->officeManager->getActive()->order('name');
        $this->template->shiftTypes = $this->shiftTypeManager->getActive()->order('start');
        if(!isset($this->template->date)) {
            $this->kek(DateTime::from($id == null ? new DateTime() : new DateTime($id)));
        }

        $t = $this->templateFactory->createTemplate();
        $t->setFile(__DIR__ . '/templates/Homepage/exportPdf.latte');
        $t->offices = $this->template->offices;
        $t->shiftTypes = $this->template->shiftTypes;
        $t->shifts = $this->template->shifts;
        $t->first = $this->template->first;
        $t->last = $this->template->last;

        $pdf = new mPDF();
        $pdf->SetTitle('Směny ' . $this->template->first->format('d. m. Y') . ' - ' . $this->template->last->format('d. m. Y'));
        $pdf->ignore_invalid_utf8 = true;
        $pdf->AddPage('L');
        $pdf->WriteHTML(file_get_contents(__DIR__ . '/../../www/plugins/kv-mpdf-bootstrap.css'), 1);
        $pdf->WriteHTML($t, 2);
        $pdf->Output('shifts_' . $this->template->first->format('d-m-Y') . '_' . $this->template->first->format('d-m-Y') . '.pdf', 'I');
        $this->redirect('this');
    }
}
