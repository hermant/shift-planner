<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Forms\UserCreateFactory;
use App\Forms\UserEditFactory;
use App\Model\ShiftManager;
use App\Model\ShiftMonthLimitManager;
use App\Model\ShiftTypeManager;
use App\Model\UserManager;
use App\Model\UserNotFoundException;
use Nette;
use Nette\Application\UI\Form;
use Nette\Database\Table\ActiveRow;
use Nette\Utils\DateTime;

class UserPresenter extends BasePresenter
{
    /** @var UserManager */
    private $userManager;

    /** @var ShiftManager */
    private $shiftManager;

    /** @var UserCreateFactory */
    private $userCreateFactory;

    /** @var UserEditFactory */
    private $userEditFactory;

    /** @var ShiftTypeManager */
    private $shiftTypeManager;

    /** @var ShiftMonthLimitManager */
    private $shiftMonthLimitManager;

    public function __construct(UserManager $userManager, UserCreateFactory $userCreateFactory, UserEditFactory $userEditFactory, ShiftTypeManager $shiftTypeManager, ShiftMonthLimitManager $shiftMonthLimitManager, ShiftManager $shiftManager)
    {
        $this->userManager = $userManager;
        $this->userCreateFactory = $userCreateFactory;
        $this->userEditFactory = $userEditFactory;
        $this->shiftTypeManager = $shiftTypeManager;
        $this->shiftMonthLimitManager = $shiftMonthLimitManager;
        $this->shiftManager = $shiftManager;
    }

    public function renderDefault() {
        $this->template->users = $this->userManager->getTable()->order('active DESC, lastname, firstname');
        $this->template->addFilter('fullname', function (?ActiveRow $user) {
            return $this->userManager->getFullName($user);
        });
    }

    public function handleDeactivate(string $id) {
        try {
            $this->userManager->deactivate(intval($id));
            if($this->isAjax()) {
                $this->redrawControl('usersContainer');
            }
        } catch (UserNotFoundException $e) {

        }
    }

    public function renderCreate() {
        $this->template->shiftTypes = $this->shiftTypeManager->getActive()->order('start');
    }

    public function renderEdit(string $id) {
        $this->template->shiftTypes = $this->shiftTypeManager->getActive()->order('start');

        $user = $this->userManager->get(intval($id));
        if (!$user) {
            $this->error('Uživatel nebyl nalezen!');
        }
        $data = $user->toArray();
        foreach($this->template->shiftTypes as $type) {
            $data['limit' . $type->id] = $this->shiftMonthLimitManager->get(intval($id), $type->id)->quantity;
        }

        $this['userEditForm']->setDefaults($data);

        $this->template->shiftTypes = $this->shiftTypeManager->getActive()->order('start');
        $this->template->dates = array();
        $date = new DateTime('now');
        $date->modify('first day of previous month');
        $date->modify('-2 months');
        $this->template->dates[] = clone $date;
        $date->modify('first day of next month');
        $this->template->dates[] = clone $date;
        $date->modify('first day of next month');
        $this->template->dates[] = clone $date;
        $date->modify('first day of next month');
        $this->template->dates[] = clone $date;
        $date->modify('first day of next month');
        $this->template->dates[] = clone $date;

        $this->template->addFilter('current', function (int $shiftTypeId, \DateTime $date) use ($id) {
            return $this->shiftManager->getMonthWorkingDays(intval($id), $shiftTypeId, $date);
        });

        $this->template->addFilter('optimal', function (int $shiftTypeId, \DateTime $date) use ($id) {
            return $this->shiftMonthLimitManager->getOptimalMonthWorkingDaysPerType(intval($date->format('Y')), intval($date->format('m')), $shiftTypeId, intval($id));
        });
    }

    protected function createComponentUserCreateForm(): Form
    {
        return $this->userCreateFactory->create(function (): void {
            $this->redirect('User:');
        });
    }

    protected function createComponentUserEditForm(): Form
    {
        $id = $this->getParameter('id');
        return $this->userEditFactory->create(intval($id), function (): void {
            $this->redirect('User:');
        });
    }



}