<?php

declare(strict_types=1);

namespace App\Model;

use Nette;

class OfficeManager
{
    use Nette\SmartObject;

    private const
        TABLE_NAME = 'office',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'name',
        COLUMN_ACTIVE = 'active';

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    function getTable() : Nette\Database\Table\Selection {
        return $this->database->table(self::TABLE_NAME);
    }

    public function get(int $id) : ?Nette\Database\Table\ActiveRow {
        return $this->getTable()->get($id);
    }

    /**
     * @param int $id
     * @throws OfficeNotFoundException
     */
    public function deactivate(int $id) {
        $office = $this->get($id);
        if(!$office) {
            throw new OfficeNotFoundException();
        }
        $office->update([
            self::COLUMN_ACTIVE => $office[self::COLUMN_ACTIVE] == 1 ? 0 : 1,
        ]);
    }

    /**
     * @param string $name
     * @throws DuplicateNameException
     */
    public function add(string $name) {
        try {
            $this->getTable()->insert([
                self::COLUMN_NAME => $name
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    /**
     * @param int $id
     * @param string $name
     * @throws OfficeNotFoundException
     */
    public function update(int $id, string $name) {
        $office = $this->get($id);
        if(!$office) {
            throw new OfficeNotFoundException();
        }
        try {
            $office->update([
                self::COLUMN_NAME => $name
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }

    public function getActive() : Nette\Database\Table\Selection {
        return $this->getTable()->where(self::COLUMN_ACTIVE, 1);
    }

}

class OfficeNotFoundException extends \Exception
{
}