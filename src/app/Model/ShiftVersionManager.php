<?php

declare(strict_types=1);

namespace App\Model;

use Nette;

class ShiftVersionManager
{
    const STANDARD = 1;
    const ONLY_SISTER = 2;
    const RECEPTIONIST = 3;

    use Nette\SmartObject;

    private const
        TABLE_NAME = 'shift_version',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'name';

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function getTable() : Nette\Database\Table\Selection {
        return $this->database->table(self::TABLE_NAME);
    }
}