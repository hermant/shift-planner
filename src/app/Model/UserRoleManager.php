<?php

declare(strict_types=1);

namespace App\Model;

use Nette;

class UserRoleManager
{
    use Nette\SmartObject;

    private const
        TABLE_NAME = 'user_role',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'name',
        COLUMN_DISPLAY_NAME = 'display_name';

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    function getTable() : Nette\Database\Table\Selection {
        return $this->database->table(self::TABLE_NAME);
    }
}