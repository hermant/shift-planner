<?php

declare(strict_types=1);

namespace App\Model;

use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
final class UserManager implements Nette\Security\IAuthenticator
{
	use Nette\SmartObject;

	private const
		TABLE_NAME = 'employee',
		COLUMN_ID = 'id',
		COLUMN_PASSWORD_HASH = 'password',
		COLUMN_EMAIL = 'email',
		COLUMN_ROLE = 'user_role',
        COLUMN_ROLE_ID = 'user_role_id',
        COLUMN_FIRSTNAME = 'firstname',
        COLUMN_LASTNAME = 'lastname',
        COLUMN_TITLES_BEFORE = 'titles_before',
        COLUMN_TITLES_AFTER = 'titles_after',
        COLUMN_ACTIVE = 'active';


	/** @var Nette\Database\Context */
	private $database;

	/** @var Passwords */
	private $passwords;

    /** @var UserRoleManager */
    private $userRoleManager;


	public function __construct(Nette\Database\Context $database, Passwords $passwords, UserRoleManager $userRoleManager)
	{
		$this->database = $database;
		$this->passwords = $passwords;
		$this->userRoleManager = $userRoleManager;
	}

    function getTable() : Nette\Database\Table\Selection {
        return $this->database->table(self::TABLE_NAME);
    }

	/**
	 * Performs an authentication.
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials): Nette\Security\IIdentity
	{
		[$email, $password] = $credentials;

		$row = $this->database->table(self::TABLE_NAME)
			->where(self::COLUMN_EMAIL, $email)
			->fetch();

		if (!$row) {
			throw new Nette\Security\AuthenticationException('Nesprávný email', self::IDENTITY_NOT_FOUND);

		} elseif (!$this->passwords->verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
			throw new Nette\Security\AuthenticationException('Nesprávné heslo', self::INVALID_CREDENTIAL);

		} elseif ($this->passwords->needsRehash($row[self::COLUMN_PASSWORD_HASH])) {
			$row->update([
				self::COLUMN_PASSWORD_HASH => $this->passwords->hash($password),
			]);
		} elseif($row[self::COLUMN_ACTIVE] == 0) {
            throw new DeactivatedAccountException();
        }

		$arr = $row->toArray();
		unset($arr[self::COLUMN_PASSWORD_HASH]);
		return new Nette\Security\Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE]->name, $arr);
	}


    /**
     * Adds new user.
     * @throws DuplicateNameException
     * @throws Nette\Utils\AssertionException
     */
	public function add(string $email,
                        string $password,
                        string $firstname,
                        string $lastname,
                        ?string $titlesBefore,
                        ?string $titlesAfter,
                        int $roleId): Nette\Database\Table\ActiveRow
	{
		Nette\Utils\Validators::assert($email, 'email');
		try {
			return $this->database->table(self::TABLE_NAME)->insert([
				self::COLUMN_EMAIL => $email,
				self::COLUMN_PASSWORD_HASH => $this->passwords->hash($password),
                self::COLUMN_FIRSTNAME => $firstname,
                self::COLUMN_LASTNAME => $lastname,
                self::COLUMN_TITLES_BEFORE => $titlesBefore,
                self::COLUMN_TITLES_AFTER => $titlesAfter,
                self::COLUMN_ROLE_ID => $roleId
			]);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
	}

	public function getFullName(Nette\Database\Table\ActiveRow $user) : string {
	    return $user[self::COLUMN_TITLES_BEFORE] . ' ' . $user[self::COLUMN_FIRSTNAME] . ' ' . $user[self::COLUMN_LASTNAME] . ' ' . $user[self::COLUMN_TITLES_AFTER];
    }

    public function get(int $id) {
	    return $this->getTable()->get($id);
    }

    /**
     * @param int $id
     * @throws UserNotFoundException
     */
    public function delete(int $id) {
        $user = $this->get($id);
        if(!$user) {
            throw new UserNotFoundException();
        }
        $user->delete();
    }

    /**
     * @param int $id
     * @throws UserNotFoundException
     */
    public function deactivate(int $id) {
        $user = $this->get($id);
        if(!$user) {
            throw new UserNotFoundException();
        }
        $user->update([
            self::COLUMN_ACTIVE => $user[self::COLUMN_ACTIVE] == 1 ? 0 : 1,
        ]);
    }

    /**
     * @param int $id
     * @param string $email
     * @param string|null $password
     * @param string $firstname
     * @param string $lastname
     * @param string|null $titlesBefore
     * @param string|null $titlesAfter
     * @param int $roleId
     * @throws UserNotFoundException
     */
    public function update(int $id,
                           string $email,
                           ?string $password,
                           string $firstname,
                           string $lastname,
                           ?string $titlesBefore,
                           ?string $titlesAfter,
                           int $roleId) {
        $data = [
            self::COLUMN_EMAIL => $email,
            self::COLUMN_FIRSTNAME => $firstname,
            self::COLUMN_LASTNAME => $lastname,
            self::COLUMN_ROLE_ID => $roleId,
            self::COLUMN_TITLES_BEFORE => $titlesBefore,
            self::COLUMN_TITLES_AFTER => $titlesAfter,
        ];
        if(!empty($password)) {
            $data['password'] = $this->passwords->hash($password);
        }

        $user = $this->get($id);
        if(!$user) {
            throw new UserNotFoundException();
        }
        $user->update($data);
    }

    public function getDoctors() : Nette\Database\Table\Selection {
        return $this->getTable()->where(self::COLUMN_ROLE_ID, $this->userRoleManager->getTable()->where('name = ?', 'doctor'));
    }

    public function getNurses() : Nette\Database\Table\Selection {
        return $this->getTable()->where(self::COLUMN_ROLE_ID, $this->userRoleManager->getTable()->where('name = ?', 'nurse'));
    }

    public function getReceptionists() : Nette\Database\Table\Selection {
        return $this->getTable()->where(self::COLUMN_ROLE_ID, $this->userRoleManager->getTable()->where('name = ?', 'receptionist'));
    }
}



class DuplicateNameException extends \Exception
{
}

class UserNotFoundException extends \Exception
{
}

class DeactivatedAccountException extends \Exception
{
}
