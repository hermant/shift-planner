<?php

declare(strict_types=1);

namespace App\Model;

use Nette;

class DeadlineManager
{
    use Nette\SmartObject;

    private const
        TABLE_NAME = 'deadline',
        COLUMN_ID = 'id',
        COLUMN_FINISHED_UNTIL = 'finished_until';

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    function getTable() : Nette\Database\Table\Selection {
        return $this->database->table(self::TABLE_NAME);
    }

}