<?php

declare(strict_types=1);

namespace App\Model;

use Nette;

class ShiftTypeManager
{
    use Nette\SmartObject;

    private const
        TABLE_NAME = 'shift_type',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'name',
        COLUMN_START = 'start',
        COLUMN_END = 'end',
        COLUMN_ACTIVE = 'active';

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function getTable() : Nette\Database\Table\Selection {
        return $this->database->table(self::TABLE_NAME);
    }

    public function get(int $id) {
        return $this->getTable()->get($id);
    }

    public function getActive() {
        return $this->getTable()->where(self::COLUMN_ACTIVE, 1);
    }

    /**
     * @param string $name
     * @param Nette\Utils\DateTime $from
     * @param Nette\Utils\DateTime $to
     * @throws ShiftOverlappingException
     */
    public function add(string $name, Nette\Utils\DateTime $from, Nette\Utils\DateTime $to) {
        if($this->getTable()->where('CAST(DATE_FORMAT(' . self::COLUMN_START . ', "%H%i") AS UNSIGNED)' . ' < ?', $to->format('Gi'))->where('CAST(DATE_FORMAT(' . self::COLUMN_END . ', "%H%i") AS UNSIGNED)' . ' > ?', $from->format('Gi'))->fetch()) {
            throw new ShiftTypeOverlappingException();
        }
        $this->getTable()->insert([
            self::COLUMN_NAME => $name,
            self::COLUMN_START => $from,
            self::COLUMN_END => $to
        ]);
    }

    public function update(int $id, string $name, Nette\Utils\DateTime $from, Nette\Utils\DateTime $to) {
        $type = $this->get($id);
        if(!$type) {
            throw new ShiftTypeNotFoundException();
        }

        if($this->getTable()->where(self::COLUMN_ID . ' != ?', $id)->where('CAST(DATE_FORMAT(' . self::COLUMN_START . ', "%H%i") AS UNSIGNED)' . ' < ?', $to->format('Gi'))->where('CAST(DATE_FORMAT(' . self::COLUMN_END . ', "%H%i") AS UNSIGNED)' . ' > ?', $from->format('Gi'))->fetch()) {
            throw new ShiftTypeOverlappingException();
        }

        $type->update([
            self::COLUMN_NAME => $name,
            self::COLUMN_START => $from,
            self::COLUMN_END => $to
        ]);
    }

    public function delete(int $id) {
        $this->get($id)->delete();
    }

    public function deactivate(int $id) {
        $type = $this->get($id);
        if(!$type) {
            throw new UserNotFoundException();
        }
        $type->update([
            self::COLUMN_ACTIVE => $type[self::COLUMN_ACTIVE] == 1 ? 0 : 1,
        ]);
    }
}

class ShiftTypeNotFoundException extends \Exception
{
}

class ShiftTypeOverlappingException extends \Exception
{
}