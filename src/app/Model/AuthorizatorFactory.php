<?php

declare(strict_types=1);

namespace App\Model;

use Nette,
    Nette\Security\Permission;

final class AuthorizatorFactory
{
    /**
     * @return Nette\Security\Permission
     */
    public static function create()
    {
        $acl = new Permission;

        $acl->addRole('guest');

        $acl->addResource('Error4xx');
        $acl->addResource('Sign');
        $acl->addResource('Homepage');
        $acl->addResource('Shift');
        $acl->addResource('User');
        $acl->addResource('Office');
        $acl->addResource('ShiftType');

        $acl->allow('guest', 'Error4xx', Permission::ALL);
        $acl->allow('guest', 'Sign', Permission::ALL);

        $acl->addRole('doctor', 'guest');
        $acl->allow('doctor', 'Homepage', Permission::ALL);
        $acl->allow('doctor', 'Shift', 'view');
        $acl->allow('doctor', 'Shift', 'interval');
        $acl->allow('doctor', 'Shift', 'sign');
        $acl->allow('doctor', 'Shift', 'signOut');
        $acl->allow('doctor', 'Shift', 'signInOut');

        $acl->addRole('nurse', 'doctor');
        $acl->addRole('receptionist', 'doctor');

        $acl->addRole('admin');
        $acl->allow('admin', Permission::ALL, Permission::ALL);

        return $acl;
    }
}