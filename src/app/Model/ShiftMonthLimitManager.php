<?php

declare(strict_types=1);

namespace App\Model;

use Nette;

class ShiftMonthLimitManager
{
    use Nette\SmartObject;

    private const
        TABLE_NAME = 'shift_month_limit',
        COLUMN_EMPLOYEE_ID = 'employee_id',
        COLUMN_SHIFT_TYPE_ID = 'shift_type_id',
        COLUMN_QUANTITY = 'quantity';

    /** @var Nette\Database\Context */
    private $database;

    /** @var ShiftTypeManager */
    private $shiftTypeManager;

    public function __construct(Nette\Database\Context $database, ShiftTypeManager $shiftTypeManager)
    {
        $this->database = $database;
        $this->shiftTypeManager = $shiftTypeManager;
    }

    public function getTable() : Nette\Database\Table\Selection {
        return $this->database->table(self::TABLE_NAME);
    }

    public function get(int $employeeId, int $shiftTypeId) : Nette\Database\Table\ActiveRow {
        $row = $this->getTable()->where(self::COLUMN_EMPLOYEE_ID, $employeeId)->where(self::COLUMN_SHIFT_TYPE_ID, $shiftTypeId)->fetch();
        if(!$row) {
            $this->add($employeeId, $shiftTypeId, -1);
            $row = $this->getTable()->where(self::COLUMN_EMPLOYEE_ID, $employeeId)->where(self::COLUMN_SHIFT_TYPE_ID, $shiftTypeId)->fetch();
        }
        return $row;
    }

    public function add(int $employeeId, int $shiftTypeId, int $quantity) : void {
        $this->getTable()->insert([
            self::COLUMN_EMPLOYEE_ID => $employeeId,
            self::COLUMN_SHIFT_TYPE_ID => $shiftTypeId,
            self::COLUMN_QUANTITY => $quantity
        ]);
    }

    public function update(int $employeeId, int $shiftTypeId, int $quantity) : void {
        $this->get($employeeId, $shiftTypeId)->update([
            self::COLUMN_QUANTITY => $quantity
        ]);
    }

    public function countDays(int $year, int $month, array $ignore) {
        $count = 0;
        $counter = mktime(0, 0, 0, $month, 1, $year);
        while (date("n", $counter) == $month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        return $count;
    }

    public function numberOfFridays(int $year, int $month) {
        return $this->countDays($year, $month, array(0, 1, 2, 3, 4, 6));
    }

    public function getOptimalMonthWorkingHours(int $year, int $month) : int {
        return $this->countDays($year, $month, array(0, 6)) * 8;
    }

    public function getOptimalMonthWorkingDays(int $year, int $month) : int {
        return $this->countDays($year, $month, array(0, 6));
    }

    public function getOptimalMonthWorkingHoursPerType(int $year, int $month, $shiftTypeId, $userId) : int {
        $limit = $this->get($userId, $shiftTypeId)->quantity;
        if($limit == -1) {
            return (int)ceil($this->getOptimalMonthWorkingHours($year, $month) / $this->shiftTypeManager->getActive()->count());
        } else {
            return (int)$limit;
        }

    }

    public function getOptimalMonthWorkingDaysPerType(int $year, int $month, $shiftTypeId, $userId) : int {
        $limit = $this->get($userId, $shiftTypeId)->quantity;
        if($limit == -1) {
            return (int)ceil($this->getOptimalMonthWorkingDays($year, $month) / $this->shiftTypeManager->getActive()->count());
        } else {
            return (int)$limit;
        }

    }

    public function getOptimalFridayShiftsPerType(int $year, int $month) {
        return $this->numberOfFridays($year, $month) / $this->shiftTypeManager->getActive()->count('*');
    }

}