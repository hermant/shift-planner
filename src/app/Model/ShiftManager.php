<?php

declare(strict_types=1);

namespace App\Model;

use Nette;

class ShiftManager
{
    use Nette\SmartObject;

    private const
        TABLE_NAME = 'shift',
        COLUMN_ID = 'id',
        COLUMN_FROM = 'working_hours_from',
        COLUMN_TO = 'working_hours_to',
        COLUMN_OFFICE_ID = 'office_id',
        COLUMN_DOCTOR = 'doctor',
        COLUMN_NURSE = 'nurse',
        COLUMN_RECEPTIONIST = 'receptionist',
        COLUMN_SHIFT_TYPE_ID = 'shift_type_id',
        COLUMN_SHIFT_VERSION = 'shift_version_id';

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    function getTable() : Nette\Database\Table\Selection {
        return $this->database->table(self::TABLE_NAME);
    }

    function getAll(Nette\Utils\DateTime $start, Nette\Utils\DateTime $end) : Nette\Database\Table\Selection {
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_FROM . '> ?', $start)->where(self::COLUMN_TO . '< ?', $end);
    }

    function getAllVisible(Nette\Utils\DateTime $start, Nette\Utils\DateTime $end) : Nette\Database\Table\Selection {
        $now = Nette\Utils\DateTime::from(0);
        $now->modify('+3 days');
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_FROM . '> ?', $start)->where(self::COLUMN_TO . '< ?', $end)->where('MONTH(' .self::COLUMN_FROM . ') <= ? AND YEAR(' . self::COLUMN_FROM . ') <= ?', $now->format('m'), $now->format('Y'));
    }

    function getAllGroupedByType(Nette\Utils\DateTime $start, Nette\Utils\DateTime $end) : Nette\Database\Table\Selection {
        return $this->getAll($start, $end)->group(self::COLUMN_SHIFT_TYPE_ID . ', YEAR(' . self::COLUMN_FROM . '), ' . 'MONTH(' . self::COLUMN_FROM . '), ' . 'DAY(' . self::COLUMN_FROM . ')')->order('shift_type.start');
    }

    function getAllOnDay(Nette\Utils\DateTime $date, int $shiftTypeId) : Nette\Database\Table\Selection {
        return $this->getTable()->where(self::COLUMN_SHIFT_TYPE_ID, $shiftTypeId)
            ->where('YEAR(' . self::COLUMN_FROM . ') = ?', $date->format('Y'))
            ->where('MONTH(' . self::COLUMN_FROM . ') = ?', $date->format('m'))
            ->where('DAY(' . self::COLUMN_FROM . ') = ?', $date->format('d'));
    }

    function isFullDoctors(Nette\Utils\DateTime $date, int $shiftTypeId) {
        return $this->getAllOnDay($date, $shiftTypeId)->select('SUM(ISNULL(' . self::COLUMN_DOCTOR . ')) AS not_signed')->where(self::COLUMN_SHIFT_VERSION, ShiftVersionManager::STANDARD)->fetch()->not_signed == 0;
    }

    function isFullNurses(Nette\Utils\DateTime $date, int $shiftTypeId) {
        return $this->getAllOnDay($date, $shiftTypeId)->select('SUM(ISNULL(' . self::COLUMN_NURSE . ')) AS not_signed')->where(self::COLUMN_SHIFT_VERSION, [ShiftVersionManager::STANDARD, ShiftVersionManager::ONLY_SISTER])->fetch()->not_signed == 0;
    }

    function isFullReceptionists(Nette\Utils\DateTime $date, int $shiftTypeId) {
        return $this->getAllOnDay($date, $shiftTypeId)->select('SUM(ISNULL(' . self::COLUMN_RECEPTIONIST . ')) AS not_signed')->where(self::COLUMN_SHIFT_VERSION, ShiftVersionManager::RECEPTIONIST)->fetch()->not_signed == 0;
    }

    function isFullAll(Nette\Utils\DateTime $date, int $shiftTypeId) {
        $result = true;
        foreach([ShiftVersionManager::STANDARD, ShiftVersionManager::ONLY_SISTER, ShiftVersionManager::RECEPTIONIST] as $ver)
        {
            if(!$this->isFullAllVersion($date, $shiftTypeId, $ver)) {
                $result = false;
            }
        }
        return $result;
    }

    function isFullAllVersion(Nette\Utils\DateTime $date, int $shiftTypeId, int $shiftVersionTypeId) {
        switch($shiftVersionTypeId) {
            case ShiftVersionManager::STANDARD:
                $selection = $this->getAllOnDay($date, $shiftTypeId)->select('SUM(ISNULL(' . self::COLUMN_DOCTOR . ')) AS not_signed_doctors, ' . 'SUM(ISNULL(' . self::COLUMN_NURSE . ')) AS not_signed_nurses')->where(self::COLUMN_SHIFT_VERSION, ShiftVersionManager::STANDARD)->fetch();
                return $selection->not_signed_doctors == 0 && $selection->not_signed_nurses == 0;
            case ShiftVersionManager::ONLY_SISTER:
                $selection = $this->getAllOnDay($date, $shiftTypeId)->select('SUM(ISNULL(' . self::COLUMN_NURSE . ')) AS not_signed_nurses')->where(self::COLUMN_SHIFT_VERSION, [ShiftVersionManager::STANDARD, ShiftVersionManager::ONLY_SISTER])->fetch();
                return $selection->not_signed_nurses == 0;
            case ShiftVersionManager::RECEPTIONIST:
                $selection = $this->getAllOnDay($date, $shiftTypeId)->select('SUM(ISNULL(' . self::COLUMN_RECEPTIONIST . ')) AS not_signed_receptionists')->where(self::COLUMN_SHIFT_VERSION, ShiftVersionManager::RECEPTIONIST)->fetch();
                return $selection->not_signed_receptionists == 0;
        }
        return false;
    }

    function isSigned(Nette\Utils\DateTime $date, int $shiftTypeId, int $userId) {
        $selection = $this->getAllOnDay($date, $shiftTypeId)->where('(' . self::COLUMN_NURSE . ' = ? OR ' . self::COLUMN_DOCTOR . ' = ? OR ' . self::COLUMN_RECEPTIONIST . ' = ?)', $userId, $userId, $userId)->select('SUM(' . self::COLUMN_DOCTOR . ') AS signed_doctors, ' . 'SUM(' . self::COLUMN_NURSE . ') AS signed_nurses,' . 'SUM(' . self::COLUMN_RECEPTIONIST . ') AS signed_receptionists')->fetch();
        return $selection->signed_doctors > 0 || $selection->signed_nurses > 0 || $selection->signed_receptionists > 0;
    }

    function getSigned(Nette\Utils\DateTime $date, int $shiftTypeId, int $userId) : Nette\Database\Table\ActiveRow {
        return $this->getAllOnDay($date, $shiftTypeId)->where('(' . self::COLUMN_NURSE . ' = ? OR ' . self::COLUMN_DOCTOR . ' = ? OR ' . self::COLUMN_RECEPTIONIST . ' = ?)', $userId, $userId, $userId)->fetch();
    }

    function getNotSignedDoctor(Nette\Utils\DateTime $date, int $shiftTypeId, int $userId) : ?Nette\Database\Table\ActiveRow {
        return $this->getAllOnDay($date, $shiftTypeId)->where(self::COLUMN_SHIFT_VERSION, [ShiftVersionManager::STANDARD])->where(self::COLUMN_DOCTOR . ' IS NULL')->fetch();
    }

    function getNotSignedNurse(Nette\Utils\DateTime $date, int $shiftTypeId, int $userId) : ?Nette\Database\Table\ActiveRow {
        return $this->getAllOnDay($date, $shiftTypeId)->where(self::COLUMN_SHIFT_VERSION, [ShiftVersionManager::STANDARD, ShiftVersionManager::ONLY_SISTER])->where(self::COLUMN_NURSE . ' IS NULL')->fetch();
    }

    function getNotSignedReceptionist(Nette\Utils\DateTime $date, int $shiftTypeId, int $userId) : ?Nette\Database\Table\ActiveRow {
        return $this->getAllOnDay($date, $shiftTypeId)->where(self::COLUMN_SHIFT_VERSION, [ShiftVersionManager::RECEPTIONIST])->where(self::COLUMN_RECEPTIONIST . ' IS NULL')->fetch();
    }

    function isSignedExact(int $shiftId, int $userId) {
        $shift = $this->get($shiftId);

        if($shift->doctor == $userId) {
            return true;
        }

        if($shift->nurse == $userId) {
            return true;
        }

        return false;
    }

    public function get(int $id) : ?Nette\Database\Table\ActiveRow {
        return $this->getTable()->get($id);
    }

    /**
     * @param Nette\Utils\DateTime $from
     * @param Nette\Utils\DateTime $to
     * @param int $officeId
     * @throws ShiftOverlappingException
     */
    public function add(Nette\Utils\DateTime $from, Nette\Utils\DateTime $to, int $officeId, int $shiftTypeId, int $shiftVersionId) : void {
        if($this->getTable()->where(self::COLUMN_OFFICE_ID, $officeId)->where(self::COLUMN_FROM . ' < ?', $to)->where(self::COLUMN_TO . ' > ?', $from)->fetch()) {
            throw new ShiftOverlappingException();
        }

        $this->getTable()->insert([
            self::COLUMN_FROM => $from,
            self::COLUMN_TO => $to,
            self::COLUMN_OFFICE_ID => $officeId,
            self::COLUMN_SHIFT_TYPE_ID => $shiftTypeId,
            self::COLUMN_SHIFT_VERSION =>$shiftVersionId
        ]);
    }

    /**
     * @param int $id
     * @param Nette\Utils\DateTime $from
     * @param Nette\Utils\DateTime $to
     * @param int|null $doctor
     * @param int|null $nurse
     * @throws ShiftNotFoundException
     */
    public function update(int $id, Nette\Utils\DateTime $from, Nette\Utils\DateTime $to, ?int $doctor, ?int $nurse, ?int $receptionist) : void {
        $shift = $this->get($id);
        if(!$shift) {
            throw new ShiftNotFoundException();
        }

        $shift->update([
            self::COLUMN_FROM => $from,
            self::COLUMN_TO => $to,
            self::COLUMN_DOCTOR => $doctor,
            self::COLUMN_NURSE => $nurse,
            self::COLUMN_RECEPTIONIST => $receptionist
        ]);
    }

    public function signDoctor(int $id, ?int $doctorId) : void {
        $shift = $this->get($id);
        if(!$shift) {
            throw new ShiftNotFoundException();
        }
        $shift->update([
            self::COLUMN_DOCTOR => $doctorId
        ]);
    }

    public function signDoctorType(Nette\Utils\DateTime $date, int $shiftTypeId, ?int $doctorId) : void {
        $shift = $this->getNotSignedDoctor($date, $shiftTypeId, $doctorId);
        if(!$shift) {
            throw new ShiftNotFoundException();
        }
        $shift->update([
            self::COLUMN_DOCTOR => $doctorId
        ]);
    }

    public function signNurse(int $id, ?int $nurseId) : void {
        $shift = $this->get($id);
        if(!$shift) {
            throw new ShiftNotFoundException();
        }
        $shift->update([
            self::COLUMN_NURSE => $nurseId
        ]);
    }

    public function signNurseType(Nette\Utils\DateTime $date, int $shiftTypeId, ?int $nurseId) : void {
        $shift = $this->getNotSignedNurse($date, $shiftTypeId, $nurseId);
        if(!$shift) {
            throw new ShiftNotFoundException();
        }
        $shift->update([
            self::COLUMN_NURSE => $nurseId
        ]);
    }

    public function signReceptionist(int $id, ?int $receptionistId) : void {
        $shift = $this->get($id);
        if(!$shift) {
            throw new ShiftNotFoundException();
        }
        $shift->update([
            self::COLUMN_RECEPTIONIST => $receptionistId
        ]);
    }

    public function signReceptionistType(Nette\Utils\DateTime $date, int $shiftTypeId, ?int $receptionistId) : void {
        $shift = $this->getNotSignedReceptionist($date, $shiftTypeId, $receptionistId);
        if(!$shift) {
            throw new ShiftNotFoundException();
        }
        $shift->update([
            self::COLUMN_RECEPTIONIST => $receptionistId
        ]);
    }

    public function checkDupe(Nette\Utils\DateTime $date, int $shiftTypeId, int $officeId) : bool {
        $shift = $this->getTable()
            ->where(self::COLUMN_SHIFT_TYPE_ID, $shiftTypeId)
            ->where(self::COLUMN_OFFICE_ID, $officeId)
            ->where('YEAR(' . self::COLUMN_FROM . ') = ?', $date->format('Y'))
            ->where('MONTH(' . self::COLUMN_FROM . ') = ?', $date->format('m'))
            ->where('DAY(' . self::COLUMN_FROM . ') = ?', $date->format('d'))->fetch();

        return $shift ? true : false;
    }

    public function deleteDoctor(int $id) : void {
        $this->get($id)->update([
           self::COLUMN_DOCTOR => null
        ]);
    }

    public function deleteNurse(int $id) : void {
        $this->get($id)->update([
            self::COLUMN_NURSE => null
        ]);
    }

    public function deleteReceptionist(int $id) : void {
        $this->get($id)->update([
            self::COLUMN_RECEPTIONIST => null
        ]);
    }

    public function getMonthWorkingHours(int $userId, int $shiftTypeId, \DateTime $date) {
        $row = $this->getTable()->select('SUM(TIMESTAMPDIFF(MINUTE, ' . self::COLUMN_FROM . ', ' . self::COLUMN_TO .  ')) AS result')
            ->where('(' . self::COLUMN_DOCTOR . ' = ? OR ' . self::COLUMN_NURSE . ' = ? OR ' . self::COLUMN_RECEPTIONIST . ' = ?) AND ' . self::COLUMN_SHIFT_TYPE_ID . ' = ?', $userId, $userId, $userId, $shiftTypeId)
            ->where('YEAR(' . self::COLUMN_FROM . ') = ?', $date->format('Y'))
            ->where('MONTH(' . self::COLUMN_FROM . ') = ?', $date->format('m'))->group(self::COLUMN_SHIFT_TYPE_ID)->fetch();
        if($row) {
            return $row->result / 60;
        } else {
            return 0;
        }
    }

    public function getMonthWorkingDays(int $userId, int $shiftTypeId, \DateTime $date) : int {
        $count = $this->getTable()
            ->where('(' . self::COLUMN_DOCTOR . ' = ? OR ' . self::COLUMN_NURSE . ' = ? OR ' . self::COLUMN_RECEPTIONIST . ' = ?) AND ' . self::COLUMN_SHIFT_TYPE_ID . ' = ?', $userId, $userId, $userId, $shiftTypeId)
            ->where('YEAR(' . self::COLUMN_FROM . ') = ?', $date->format('Y'))
            ->where('MONTH(' . self::COLUMN_FROM . ') = ?', $date->format('m'))->group(self::COLUMN_SHIFT_TYPE_ID)->count('*');
        return $count;
    }

    public function getFridayShiftsCount(int $userId, int $shiftTypeId, \DateTime $date) {
        return $this->getTable()
            ->where('(' . self::COLUMN_DOCTOR . ' = ? OR ' . self::COLUMN_NURSE . ' = ? OR ' . self::COLUMN_RECEPTIONIST . ' = ?) AND ' . self::COLUMN_SHIFT_TYPE_ID . ' = ?', $userId, $userId, $userId, $shiftTypeId)
            ->where('YEAR(' . self::COLUMN_FROM . ') = ?', $date->format('Y'))
            ->where('MONTH(' . self::COLUMN_FROM . ') = ?', $date->format('m'))
            ->where('DAYOFWEEK(' . self::COLUMN_FROM . ') = ?', 6)->count('*');

    }

    public function isOtherFridaysFullDoctor(int $shiftTypeId, \DateTime $date) {
        $freeCnt = $this->getTable()
            ->where('YEAR(' . self::COLUMN_FROM . ') = ?', $date->format('Y'))
            ->where('MONTH(' . self::COLUMN_FROM . ') = ?', $date->format('m'))
            ->where('DAYOFWEEK(' . self::COLUMN_FROM . ') = ?', 6)
            ->where(self::COLUMN_SHIFT_TYPE_ID . ' != ?', $shiftTypeId)
            ->where(self::COLUMN_SHIFT_VERSION, ShiftVersionManager::STANDARD)
            ->where(self::COLUMN_DOCTOR, null)
            ->count('*');

        return $freeCnt == 0;
    }

    public function isOtherFridaysFullSister(int $shiftTypeId, \DateTime $date) {
        $freeCnt = $this->getTable()
            ->where('YEAR(' . self::COLUMN_FROM . ') = ?', $date->format('Y'))
            ->where('MONTH(' . self::COLUMN_FROM . ') = ?', $date->format('m'))
            ->where('DAYOFWEEK(' . self::COLUMN_FROM . ') = ?', 6)
            ->where(self::COLUMN_SHIFT_TYPE_ID . ' != ?', $shiftTypeId)
            ->where(self::COLUMN_SHIFT_VERSION, [ShiftVersionManager::STANDARD, ShiftVersionManager::ONLY_SISTER])
            ->where(self::COLUMN_DOCTOR, null)
            ->count('*');

        return $freeCnt == 0;
    }

    public function isOtherFridaysFullReceptionist(int $shiftTypeId, \DateTime $date) {
        $freeCnt = $this->getTable()
            ->where('YEAR(' . self::COLUMN_FROM . ') = ?', $date->format('Y'))
            ->where('MONTH(' . self::COLUMN_FROM . ') = ?', $date->format('m'))
            ->where('DAYOFWEEK(' . self::COLUMN_FROM . ') = ?', 6)
            ->where(self::COLUMN_SHIFT_TYPE_ID . ' != ?', $shiftTypeId)
            ->where(self::COLUMN_SHIFT_VERSION, ShiftVersionManager::RECEPTIONIST)
            ->where(self::COLUMN_DOCTOR, null)
            ->count('*');

        return $freeCnt == 0;
    }

    public function checkMultiple(Nette\Utils\DateTime $date, int $userId) {
        $shift = $this->getTable()
            ->where('(' . self::COLUMN_NURSE . ' = ? OR ' . self::COLUMN_DOCTOR . ' = ?)', $userId, $userId)
            ->where('YEAR(' . self::COLUMN_FROM . ') = ?', $date->format('Y'))
            ->where('MONTH(' . self::COLUMN_FROM . ') = ?', $date->format('m'))
            ->where('DAY(' . self::COLUMN_FROM . ') = ?', $date->format('d'))->fetch();

        return $shift ? true : false;
    }
}

class ShiftOverlappingException extends \Exception
{
}

class ShiftNotFoundException extends \Exception
{
}