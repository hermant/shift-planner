-- Generated: 2020-09-30 16:24
-- Version: 1.1
-- Project: Shift Planner
-- Author: quvix

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER SCHEMA `shift_planner`  DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_czech_ci ;

ALTER TABLE `shift_planner`.`shift` 
ADD COLUMN `receptionist` INT(11) NULL DEFAULT NULL AFTER `doctor`,
ADD COLUMN `shift_version_id` INT(11) NOT NULL DEFAULT 1 AFTER `shift_type_id`,
ADD INDEX `fk_shift_employee1_idx` (`receptionist` ASC),
ADD INDEX `fk_shift_shift_version1_idx` (`shift_version_id` ASC);
;

ALTER TABLE `shift_planner`.`shift_type` 
ADD COLUMN `active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `end`;

CREATE TABLE IF NOT EXISTS `shift_planner`.`shift_version` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_czech_ci;

INSERT INTO `shift_planner`.`shift_version` (`id`, `name`) VALUES (1, 'Standardní');
INSERT INTO `shift_planner`.`shift_version` (`id`, `name`) VALUES (2, 'Pouze sestra');
INSERT INTO `shift_planner`.`shift_version` (`id`, `name`) VALUES (3, 'Recepce');

INSERT INTO `shift_planner`.`user_role` (`id`, `name`, `display_name`) VALUES (4, 'receptionist', 'Recepční');

DROP TABLE IF EXISTS `shift_planner`.`deadline` ;

ALTER TABLE `shift_planner`.`shift` 
ADD CONSTRAINT `fk_shift_employee1`
  FOREIGN KEY (`receptionist`)
  REFERENCES `shift_planner`.`employee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_shift_shift_version1`
  FOREIGN KEY (`shift_version_id`)
  REFERENCES `shift_planner`.`shift_version` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;




SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

